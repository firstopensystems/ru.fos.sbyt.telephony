﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Web;

namespace ru.tsb.telephony.web
{
    public class Global : System.Web.HttpApplication
    {
        private static Process ciscoConnection;

        protected void Application_Start(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            try
            {
                if (ciscoConnection != null) return;
                // Start the child process.
                ciscoConnection = new Process
                {
                    StartInfo =
                    {
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        RedirectStandardInput = true,
                        WorkingDirectory = HttpContext.Current.Server.MapPath("/"),
                        FileName = String.Format("{0}cisco.cucm.hub.bat", HttpContext.Current.Server.MapPath("/")),
                        Arguments = String.Format("\"{0}://{1}:{2}/\" \"{3}\" {4} {5}",
                                                    HttpContext.Current.Request.Url.Scheme,
                                                    HttpContext.Current.Request.Url.Host,
                                                    HttpContext.Current.Request.Url.Port,
                                                    ConfigurationManager.AppSettings["cisco:host"],
                                                    ConfigurationManager.AppSettings["cisco:user"],
                                                    ConfigurationManager.AppSettings["cisco:pass"])
                    }
                };
                // Redirect the output stream of the child process.
                ciscoConnection.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                Console.WriteLine(ex.StackTrace);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (ciscoConnection == null) return;
            ciscoConnection.StandardInput.AutoFlush = true;
            ciscoConnection.StandardInput.WriteLine('@');
            ciscoConnection.StandardInput.Close();
        }
    }
}