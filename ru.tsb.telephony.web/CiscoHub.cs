﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ru.tsb.telephony.web
{
    public class CiscoHub : Hub
    {
        public static Action<String> AddPhoneCompleteCallback;
        public static Action<String, String> IncommingCallCallback;
        public static Action<String, String> IncommingCallAnsweredCallback;
        public static Action<String> IncommingCallDisconnectedCallback;
        public static Func<String[]> GetAllPhonesCallback;

        #region methods on hub

        public void AddPhoneComplete(String phone_number)
        {
            AddPhoneCompleteCallback(phone_number);
        }

        public void IncommingCall(String origDn, String destDn)
        {
            IncommingCallCallback(origDn, destDn);
        }

        public void IncommingCallAnswered(String origDn, String destDn)
        {
            IncommingCallAnsweredCallback(origDn, destDn);
        }

        public void IncommingCallDisconnected(String dn)
        {
            IncommingCallDisconnectedCallback(dn);
        }

        public String[] GetAllPhones()
        {
            return GetAllPhonesCallback();
        }

        #endregion
    }
}