﻿using System.Collections.Generic;
using System.Linq;

namespace ru.tsb.telephony.web
{
    /// <summary>
    ///     хранилище клиент-учетка в памяти
    /// </summary>
    public class ConnectionInMemmoryStorage
    {
        private readonly Dictionary<string, HashSet<string>> _connections =
            new Dictionary<string, HashSet<string>>();

        public ConnectionInMemmoryStorage()
        {
        }

        public int Count
        {
            get { return _connections.Count; }
        }

        /// <summary>
        /// добавляем связь
        /// </summary>
        /// <param name="dn"></param>
        /// <param name="connectionId"></param>
        public void Add(string dn, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(dn, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(dn, connections);
                }
                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        /// <summary>
        /// все клиенты по учетке
        /// </summary>
        /// <param name="dn"></param>
        /// <returns></returns>
        public IEnumerable<string> GetConnections(string dn)
        {
            HashSet<string> connections;
            return _connections.TryGetValue(dn, out connections) ? connections : Enumerable.Empty<string>();
        }

        /// <summary>
        /// все учетки по клиенту
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IEnumerable<string> GetDn(string connection)
        {
            lock (_connections)
            {
                return _connections.Where(s => s.Value.Contains(connection)).Select(d => d.Key);
            }
        }

        /// <summary>
        /// все учетки
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetDn()
        {
            lock (_connections)
            {
                return _connections.Select(d => d.Key);
            }
        }

        /// <summary>
        /// исключаем связь
        /// </summary>
        /// <param name="dn"></param>
        /// <param name="connectionId"></param>
        public void Remove(string dn, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(dn, out connections)) return;

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count != 0) return;
                    _connections.Remove(dn);
                }
            }
        }
    }
}