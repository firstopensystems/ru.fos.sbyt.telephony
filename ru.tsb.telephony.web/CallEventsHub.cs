﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ru.tsb.telephony.web
{
    /// <summary>
    /// класс сервиса отслеживания входящих звонков
    /// </summary>
    public class CallEventsHub : Hub
    {
        /// <summary>
        /// здесь мы храним связь клиент - учетка
        /// </summary>
        private static readonly ConnectionInMemmoryStorage Connections =
            new ConnectionInMemmoryStorage();

        public CallEventsHub()
        {
            if (CiscoHub.AddPhoneCompleteCallback == null)
            {
                //отправляем пустое сообщение дав понять что мы начали прослушку
                CiscoHub.AddPhoneCompleteCallback = (dn) =>
                {
                    List<string> conns = Connections.GetConnections(dn).ToList();
                    // если у нас клиенты кто слушает этот звонок
                    if (conns.Any())
                    {
                        //уведомляем
                        Clients.Clients(conns).OnCall("");
                    }
                };
            }
            if (CiscoHub.IncommingCallCallback == null)
            {
                CiscoHub.IncommingCallCallback = (origDn, destDn) =>
                {
                    List<string> conns = Connections.GetConnections(destDn).ToList();
                    // если у нас клиенты кто слушает этот звонок
                    if (conns.Any())
                    {
                        //уведомляем
                        Clients.Clients(conns).OnCall(origDn);
                    }
                };
            }
            if (CiscoHub.IncommingCallAnsweredCallback == null)
            {
                CiscoHub.IncommingCallAnsweredCallback = (origDn, destDn) =>
                {
                    List<string> conns = Connections.GetConnections(destDn).ToList();
                    // если у нас клиенты кто слушает этот звонок
                    if (conns.Any())
                    {
                        //уведомляем
                        Clients.Clients(conns).OnCallAnswered(origDn, destDn);
                    }
                };
            }
            if (CiscoHub.IncommingCallDisconnectedCallback == null)
            {
                CiscoHub.IncommingCallDisconnectedCallback = (dn) =>
                {
                    List<string> conns = Connections.GetConnections(dn).ToList();
                    // если у нас клиенты кто слушает этот звонок
                    if (conns.Any())
                    {
                        //уведомляем
                        Clients.Clients(conns).OnCallDisconnected(dn);
                    }
                };
            }
            if (CiscoHub.GetAllPhonesCallback == null)
            {
                CiscoHub.GetAllPhonesCallback = () => Connections.GetDn().ToArray();
            }
        }

        /// <summary>
        /// когда клиент отключается мы удаляем регистрацию на сервере
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            foreach (string dn in Connections.GetDn(Context.ConnectionId))
            {
                Connections.Remove(dn, Context.ConnectionId);
                if (Connections.GetConnections(dn).Any()) continue;
                var ciscoHub = GlobalHost.ConnectionManager.GetHubContext<CiscoHub>();
                ciscoHub.Clients.All.deletePhone(dn);
            }
            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// При подключении клиент вызывает этот метод и мы регистрируемся на сервере телефонии для приема входящих звонков
        /// </summary>
        /// <param name="phone_number">абонент</param>
        public void Register(string phone_number)
        {
            Connections.Add(phone_number, Context.ConnectionId);
            if (Connections.GetConnections(phone_number).Count() > 1) return;
            var ciscoHub = GlobalHost.ConnectionManager.GetHubContext<CiscoHub>();
            ciscoHub.Clients.All.addPhone(phone_number);
        }

        /// <summary>
        /// вызвав этот методо клент говорит нам чтоб мы набрали номер
        /// </summary>
        /// <param name="origDn">учетка телефона с которого будет звонок</param>
        /// <param name="destDn">учетка кому мы звоним</param>
        public void Call(string origDn, string destDn)
        {
            var ciscoHub = GlobalHost.ConnectionManager.GetHubContext<CiscoHub>();
            ciscoHub.Clients.All.makeCall(origDn, destDn);
        }
    }
}