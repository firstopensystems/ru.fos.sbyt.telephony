﻿using System;
using System.Text;
using System.Net;
using System.Web.Script.Serialization;

namespace ru.tsb.sbyt.telephony.web.srv
{
    public class Loggly
    {
        public static bool Exception(String body)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "text/plain");

                var encoding = new ASCIIEncoding();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                client.UploadDataAsync(
                    new Uri("https://logs-01.loggly.com/inputs/ceb4fcf0-36b7-4233-89e5-0b4f607ecec4/tag/exception/"),
                    "POST", encoding.GetBytes(body));
                return true;
            }
        }

        public static T Json<T>(T body)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "text/plain");

                var encoding = new ASCIIEncoding();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                client.UploadDataAsync(
                    new Uri("https://logs-01.loggly.com/inputs/ceb4fcf0-36b7-4233-89e5-0b4f607ecec4/tag/json/"), "POST",
                    encoding.GetBytes(new JavaScriptSerializer().Serialize(body)));
                return body;
            }
        }

        public static String Json(String body)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "text/plain");

                var encoding = new ASCIIEncoding();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                client.UploadDataAsync(
                    new Uri("https://logs-01.loggly.com/inputs/ceb4fcf0-36b7-4233-89e5-0b4f607ecec4/tag/json/"), "POST",
                    encoding.GetBytes(body));
                return body;
            }
        }

        public static bool Error(String body)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "text/plain");

                var encoding = new ASCIIEncoding();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                client.UploadDataAsync(
                    new Uri("https://logs-01.loggly.com/inputs/ceb4fcf0-36b7-4233-89e5-0b4f607ecec4/tag/error/"), "POST",
                    encoding.GetBytes(body));
                return true;
            }
        }

        public static bool Info(String body)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "text/plain");

                var encoding = new ASCIIEncoding();
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                client.UploadDataAsync(
                    new Uri("https://logs-01.loggly.com/inputs/ceb4fcf0-36b7-4233-89e5-0b4f607ecec4/tag/info/"), "POST",
                    encoding.GetBytes(body));
                return true;
            }
        }
    }
}
