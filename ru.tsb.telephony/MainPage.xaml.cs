﻿using System;
using System.Net;
using System.Net.Browser;
using System.Windows;
using System.Windows.Controls;
using ru.tsb.sbyt.telephony.sl;

namespace ru.tsb.telephony
{
    public partial class MainPage
    {
        // храним объект связи с сервисом пока не выгрузиться форма
        private SIPClient client;
        public MainPage()
        {
            InitializeComponent();

            // http://msdn.microsoft.com/en-us/library/dd920295(v=vs.95).aspx
            // With Silverlight, you can specify whether the browser or the client provides HTTP handling for your Silverlight-based applications. 
            // By default, HTTP handling is performed by the browser and you must opt-in to client HTTP handling. 
            WebRequest.RegisterPrefix("http://", WebRequestCreator.ClientHttp);
            WebRequest.RegisterPrefix("https://", WebRequestCreator.ClientHttp);

            // инициализируем клиента signalR 
            // при переносе сервиса на другой адрес не задтье изменить схему хост порт и контекст сервиса
            client = new SIPClient(String.Format("{0}://{1}:{2}/",
                Application.Current.Host.Source.Scheme,
                Application.Current.Host.Source.Host,
                Application.Current.Host.Source.Port),
                "7103");
            // привязываем событие входящего звонка к действию в приложении
            client.OnCall += cli_OnCall;
        }

        void cli_OnCall(string sipUrl)
        {
            if (String.IsNullOrEmpty(sipUrl))
            {
                Dispatcher.BeginInvoke(() =>
                {
                    debug.Text += "Отслеживаем входящий звонок на +7 343 215 76 32 (7103)\n";
                });
            }
            else
            {
                Dispatcher.BeginInvoke(() => debug.Text += "Входящий звонок " + sipUrl + "\n");
            }
        }

        /// <summary>
        /// По нажатию на кнопку отправляем комманду на сервис SCCP CISCO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            debug.Text += "Отправили команду на сервер CISCO 99058077697\n";
            client.Call("7103", "989058077697");
        }

        private void HyperlinkButton1_Click(object sender, RoutedEventArgs e)
        {
            debug.Text += "Отправили команду на CISCO позвонить " + ((HyperlinkButton)sender).NavigateUri + "\n";
            client.Call("7103", ((HyperlinkButton)sender).NavigateUri.ToString());
        }

        // сервис корректно обрабатывает обрывы связи, но если мы правильно завершаем работу 
        // то сообщаем серверу что мы отключаемся и больше не надо отслеживать входящие звонки
        // в этом случае сервис отключитсья от IP АТС
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (client != null)
            {
                client.Unload();
            }
        }
    }
}
