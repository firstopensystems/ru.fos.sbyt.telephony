﻿using System;
using Microsoft.AspNet.SignalR.Client;

namespace ru.tsb.sbyt.telephony.sl
{

    // ReSharper disable once InconsistentNaming
    /// <summary>
    /// Класс клиента на signalR для сервиса телефонии
    /// </summary>
    public class SIPClient
    {
        /// <summary>
        /// Делегат для события входящий звонок
        /// </summary>
        /// <param name="sipUrl">Делегат получает от сервера юрл абонента входящего звонка +79058077699, 79028077699, 37453</param>
        public delegate void OnCallDelegate(String dn);

        /// <summary>
        /// Событие входящий звонок
        /// </summary>
        public event OnCallDelegate OnCall;

        // объект соединения с сервером
        private readonly HubConnection _hubConnection;
        // соединение с сервисом сервера
        private readonly IHubProxy _hubProxy;

        /// <summary>
        /// Конструктор инициализирует соединение с сервисом, подписку на событие, и отработку разрыва связи
        /// </summary>
        /// <param name="serviceUrl">адрес сервиса http://myiptel.com/</param>
        /// <param name="dn">dn абонента под которым регистрируемся на АТС</param>
        public SIPClient(string serviceUrl, string dn)
        {
            _hubConnection = new HubConnection(serviceUrl);
            _hubProxy = _hubConnection.CreateHubProxy("CallEventsHub");
            _hubProxy.On<string>("OnCall", message => OnCall(message));
            _hubConnection.Reconnected += () => _hubProxy.Invoke("Register", dn).Wait();
            _hubConnection.Start().ContinueWith(task => _hubProxy.Invoke("Register", dn).Wait());
        }

        /// <summary>
        /// Комманда набрать номер
        /// </summary>
        /// <param name="sipUrl">кому набрать номер</param>
        /// <param name="sipUrl1">куда набрать номер</param>
        /// <returns>успешность вызова</returns>
        public bool Call(string sipUrl, string sipUrl1)
        {
            _hubProxy.Invoke("Call", sipUrl, sipUrl1).Wait();
            return true;
        }

        /// <summary>
        /// Правильная выгрузка объекта
        /// </summary>
        public void Unload()
        {
            if (_hubConnection != null)
            {
                _hubConnection.Stop();
            }
        }
    }
}